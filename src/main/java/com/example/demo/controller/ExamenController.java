package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ExamenDao;
import com.example.demo.request.ExamenRequest;
import com.example.demo.service.IExamenService;

@RestController
public class ExamenController {

	@Autowired
	private IExamenService examenService;
	
	@GetMapping(path="/getExamen")
	public @ResponseBody Iterable<ExamenDao> getExamen(){
		return examenService.getExamen();
	}
	@PostMapping(path="/putExamen")
	public String putExamen(@RequestBody ExamenRequest request) {
		return examenService.putExamen(request);
	} 
}
