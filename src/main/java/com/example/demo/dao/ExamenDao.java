package com.example.demo.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PEDIDOS_W")
public class ExamenDao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private double total;
	private String date_sale;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getDate_sale() {
		return date_sale;
	}
	public void setDate_sale(String date_sale) {
		this.date_sale = date_sale;
	}
}
