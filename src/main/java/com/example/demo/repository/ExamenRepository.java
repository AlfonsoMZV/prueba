package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.dao.ExamenDao;

public interface ExamenRepository extends CrudRepository<ExamenDao, Integer>{

}
