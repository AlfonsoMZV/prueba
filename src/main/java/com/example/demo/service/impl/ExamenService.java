package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ExamenDao;
import com.example.demo.repository.ExamenRepository;
import com.example.demo.request.ExamenRequest;
import com.example.demo.service.IExamenService;

@Service
public class ExamenService implements IExamenService{

	@Autowired
	private ExamenRepository examenRepository;
	
	@Override
	public Iterable<ExamenDao> getExamen() {
		return examenRepository.findAll();
	}

	@Override
	public String putExamen(ExamenRequest request) {
		ExamenDao examenDao = new ExamenDao();
		examenDao.setTotal(request.getTotal());
		examenDao.setDate_sale(request.getDate_sale());
		examenRepository.save(examenDao);
		return "Examen Agregado";
	}

}
