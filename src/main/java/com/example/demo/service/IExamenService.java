package com.example.demo.service;

import com.example.demo.dao.ExamenDao;
import com.example.demo.request.ExamenRequest;

public interface IExamenService {

	public Iterable<ExamenDao> getExamen();
	public String putExamen(ExamenRequest request);
	
}
